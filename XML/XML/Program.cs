﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Net;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateEmptyXML();
            AddNewRandomPersons(1000);
            ShowPersons(12);

            Console.ReadLine();

        }

        private static void CreateEmptyXML()
        {
            var doc = XDocument.Parse("<Persons></Persons>");
            doc.Save("AllPersons.xml");
        }

        private static void ShowPersons(int count)
        {
            var doc = XDocument.Load("AllPersons.xml").Element("Persons");

            foreach (var pers in doc.Elements("RandomPerson"))
            {
                var name = pers.Element("name");
                Console.WriteLine($"First Name : {name.Element("first").Value}    Phone : {pers.Element("phone").Value}");
                count--;
                if (count == 0) break;
            }

        }

        private static void AddNewRandomPersons(int count)
        {
            var wc = new WebClient().DownloadString(@"http://api.randomuser.me/?format=xml&results=" + Convert.ToString(count));

            var buf = XDocument.Parse(wc);

            foreach (var pers in buf.Element("user").Elements("results"))
            {
                pers.Name = "RandomPerson";
            }
            var doc = XDocument.Load("AllPersons.xml");
            var root = doc.Element("Persons");

            foreach (var rers in buf.Element("user").Elements("RandomPerson"))
            {
                root.Add(rers);
            }

            doc.Save("AllPersons.xml");
        }

        private static void ClearPersons()
        {
            var doc = XDocument.Load("AllPersons.xml");
            doc.Element("Persons").RemoveAll();
            doc.Save("AllPersons.xml");
        }
    }
}
